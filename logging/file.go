package logging

import (
	"log"
	"os"
)

var (
	// 日志路径
	logSavePath = "runtime/logs/"
	// 日志保存名
	logSaveName = "log"
	// 后缀名
	logFileExt = "log"
	// 日期格式
	timeFormat = "20060102"
)

func (zhLogger ZhLogger) openLogFile() *os.File {
	_, err := os.Stat(zhLogger.logFileFullPath)
	switch {
	case os.IsNotExist(err):
		zhLogger.mkDir()
	case os.IsPermission(err):
		log.Fatalf("Permission :%v", err)
	}

	handle, err := os.OpenFile(zhLogger.logFileFullPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalf("Fail to OpenFile :%v", err)
	}

	return handle
}

func (zhLogger ZhLogger) mkDir() {
	dir, _ := os.Getwd()
	err := os.MkdirAll(dir+"/"+zhLogger.LogSavePath, os.ModePerm)
	if err != nil {
		panic(err)
	}
}
