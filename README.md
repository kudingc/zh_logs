引入包直接使用即可

1.首先创建日志对象

    logger := logging.ZhLogger{}

2.初始化日志对象

    logger.InitConfig()

3.日志对象字段说明

    LogSavePath         string                 // 文件保存路径
    LogSaveModel        Model                  // 日志保存模式  Default：Customize：自定义文件名，StandAlone：各类型日志分开保存
    LogSaveName         string                 // 日志保存名称
    LogLevel            Level                  // 日志级别
    LogFormatter        Format                 // 日志格式
    LogSplitModel       SplitModel             // 日志分割模式 默认按时间分割
    LogSplitTime        time.Duration          // 日志分割时长
    LogMaxSize          int                    // 日志分割大小，以MB为单位，默认大小为100M 只有LogSplitModel为Size时才生效
    LogMaxBackUp        int                    // 最大保留过期文件个数
    PrintStdout         bool                   // 是否同时将日志输出到控制台 true:输出 false:不输出（默认）
    LogAdditionalFields map[string]interface{} // 日志附加字段 该附加字段会附加到日志中输出出来

4.举例

（1）将所有类型日志保存到同一个文件中，按日志大小分割文件，每个日志文件大小为1M，日志文件最多保留两份，以json方式保存日志，日志保存到文件的同时输出到控制台

    import (
        "gitee.com/kudingc/zh_logs/logging"
    )
    func main() {
        logger := logging.ZhLogger{
            LogSavePath:   "runtime/logs/",
            LogLevel:      logging.Trace,
            LogSplitModel: logging.Size,
            LogMaxSize:    1,
            LogMaxBackUp:  2,
            LogFormatter:  logging.Json,
            LogSaveModel:  logging.Integrated,
            LogSaveName:   "gt",
            PrintStdout:   true,
        }
        logger.InitConfig()
        logger.Debug(i, "Debug日志")
        logger.Info(i, "Info日志")
    }

（2）不同类型的日志分开保存，日志文件按日期进行分割，分割日期为7天，即每7天的日志文件保存到一个日志文件中，最多保存2份，以标准方式保存日志，不输出到控制台

    logger := logging.ZhLogger{
		LogSavePath:         "runtime/logs/",
		LogLevel:            logging.Trace,
		LogSplitModel:       logging.Date,
		LogSplitTime:        time.Hour * 24 * 7,
		LogMaxBackUp:        2,
		LogFormatter:        logging.Normal,
		LogSaveModel:        logging.StandAlone,
		LogSaveName:         "gt",
	}
	logger.InitConfig()
	logger.Info(1, "Info日志")
	logger.Debug(2, "Debug日志")

（3）不同类型的日志文件分开保存，每7天的日志保存到一个日志文件中，最多保存2个日志文件，日志格式为文本格式，添加附加字段（username,password）

    m1 := map[string]interface{}{
        "username": "gt",
        "password": "123456",
    }

	logger := logging.ZhLogger{
		LogSavePath:         "runtime/logs/",
		LogLevel:            logging.Trace,
		LogSplitModel:       logging.Date,
		LogSplitTime:        time.Hour * 24 * 7,
		LogMaxBackUp:        2,
		LogFormatter:        logging.Text,
		LogSaveModel:        logging.StandAlone,
		LogSaveName:         "gt",
		LogAdditionalFields: m1,
	}
	logger.InitConfig()
	logger.Info(1, "Info日志")
	logger.Debug(2, "Debug日志")

（4）将指定的报警类型日志发送到指定邮件

	logger := logging.ZhLogger{
		LogSavePath:         "runtime/logs/",
		LogLevel:            logging.Trace,
		LogSplitModel:       logging.Date,
		LogSplitTime:        time.Hour * 24 * 7,
		LogMaxBackUp:        2,
		LogFormatter:        logging.Text,
		LogSaveModel:        logging.StandAlone,
		LogSaveName:         "gt",
	}
	logger.SendToEmail.Enable = true
	logger.SendToEmail.Title = "日志模块"
	logger.SendToEmail.SubscribeMap = map[logging.Level]string{
		logging.Error: "gaoteng@hzsswjt.com",
		logging.Debug: "gaoteng@hzsswjt.com",
	}
	logger.SendToEmail.SenderData.SenderUser = "abc@qq.com"
    // 发送方密码 为开启smtp后给的秘钥
	logger.SendToEmail.SenderData.SenderPasswd = "***********"
	logger.SendToEmail.SenderData.SenderHost = "smtp.qq.com"
	logger.SendToEmail.SenderData.SenderPort = 25
	logger.InitConfig()
	logger.Error(1, "Error日志")
	logger.Debug(2, "Debug日志")

（5）发送日志到kafka

    m1 := map[string]interface{}{
		"username": "gt",
		"password": "123456",
	}

	logger := logging.ZhLogger{
		LogSavePath:         "runtime/logs/",
		LogLevel:            logging.Trace,
		LogSplitModel:       logging.Date,
		LogSplitTime:        time.Hour * 24 * 7,
		LogMaxBackUp:        2,
		LogFormatter:        logging.Json,
		LogSaveModel:        logging.StandAlone,
		LogSaveName:         "gt",
		LogAdditionalFields: m1,
	}
	logger.SendToKafka.Enable = true
	logger.SendToKafka.Broker = []string{"11.11.11.11:9092"}
	logger.SendToKafka.Topic = "mykafka"
	logger.InitConfig()
	logger.Error(1, "Error日志")
	logger.Debug(2, "Debug日志")